# MIT License

**Copyright (c) 2018-2019 Mike K�ster (aka Xantiva)**

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

# Use of external code

## Nuget Packages

 	

~~~~
Id                                     Versions LicenseUrl                                                              
--                                     -------- ----------                                                              
Autofac                                {4.9.2}  https://licenses.nuget.org/MIT                                          
BouncyCastle                           {1.8.5}  http://www.bouncycastle.org/licence.html                                
Castle.Core                            {4.3.1}  http://www.apache.org/licenses/LICENSE-2.0.html                         
CsvHelper                              {12.1.2} https://raw.githubusercontent.com/JoshClose/CsvHelper/master/LICENSE.txt
LiteDB                                 {4.1.4}  https://raw.github.com/mbdavid/LiteDB/master/LICENSE                    
MailKit                                {2.1.3}  https://licenses.nuget.org/MIT                                          
MimeKit                                {2.1.3}  https://licenses.nuget.org/MIT                                          
Moq                                    {4.10.1} https://raw.githubusercontent.com/moq/moq4/master/License.txt           
MSTest.TestAdapter                     {1.4.0}  http://www.microsoft.com/web/webpi/eula/net_library_eula_enu.htm        
MSTest.TestFramework                   {1.4.0}  http://www.microsoft.com/web/webpi/eula/net_library_eula_enu.htm        
NETStandard.Library                    {2.0.3}                                                                          
Newtonsoft.Json                        {12.0.1} https://licenses.nuget.org/MIT                                          
NLog                                   {4.6.0}  https://github.com/NLog/NLog/blob/master/LICENSE.txt                    
System.Runtime.CompilerServices.Unsafe {4.5.2}  https://github.com/dotnet/corefx/blob/master/LICENSE.TXT                
System.Threading.Tasks.Extensions      {4.5.2}  https://github.com/dotnet/corefx/blob/master/LICENSE.TXT                
System.ValueTuple                      {4.5.0}  https://github.com/dotnet/corefx/blob/master/LICENSE.TXT                
 	

~~~~

*Create this list from within the "Nuget Package Manager Console":*

`Get-Package | Select-Object Id,Versions,LicenseUrl | Sort-Object -Property ID | Get-Unique -AsString`

