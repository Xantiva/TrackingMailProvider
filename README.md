# Tracking-Mail Provider

Konsolenanwendung zur Verteilung von Tracking-Mail der Versanddienstleister an die Kunden.

Die Anwendung liegt jetzt in Azure DevOps: https://dev.azure.com/xantiva/TrackingMailProvider

(Eignete sich besser für .NET / C# Anwendungen mit der Build Pipeline.)