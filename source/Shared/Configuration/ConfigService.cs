﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Xantiva.TrackingMailProvider.Shared.Configuration
{
    /// <summary>
    /// The configuration service which supports the <see cref="IConfigParams"/> and the folders.
    /// </summary>
    public class ConfigService : IConfigService
    {
        private readonly string filename = "config.json";

        /// <summary>
        /// The <see cref="IConfigParams"/> configuration data.
        /// </summary>
        public IConfigParams ConfigParams { get; set; }

        /// <summary>
        /// Path to the folder from where the mail addresses should be imported.
        /// </summary>
        public string Importfolder { get; private set; }

        /// <summary>
        /// Path to the folder from where the application should "work" in. Contains the import, database folder, ...
        /// </summary>
        public string Workfolder { get; private set; }

        /// <summary>
        /// Creates a new <see cref="ConfigService"/> instance.
        /// </summary>
        /// <param name="workfolder">The workfolder, which sould be used.</param>
        public ConfigService(string workfolder)
        {
            Workfolder = workfolder ?? throw new ArgumentNullException(nameof(workfolder), "The workfolder must not be NULL.");
            Importfolder = Path.Combine(Workfolder, "import");
            if (!Directory.Exists(Importfolder)) Directory.CreateDirectory(Importfolder);

            var configfolder = Path.Combine(Workfolder, "config");
            if (!Directory.Exists(configfolder)) Directory.CreateDirectory(configfolder);

            LoadConfig(configfolder);
        }

        private void LoadConfig(string configfolder)
        {
            string configfile = Path.Combine(configfolder, filename);
            if (!File.Exists(configfile)) SaveDefaultConfig(configfile);

            try
            {
                ConfigParams = JsonConvert.DeserializeObject<ConfigParams>(File.ReadAllText(configfile));
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not create default config file: {configfile}. Reason: {ex}");
                throw;
            }
        }

        private void SaveDefaultConfig(string configfile)
        {
            Trace.TraceInformation($"No config file found, trying to create one at {configfile}");
            try
            {
                var defaultConfig = new ConfigParams();
                defaultConfig.Imap = new ConnectionData()
                {
                    Host = "imap.example.com",
                    Port = 993,
                    User = "user",
                    Password = "password"
                };
                defaultConfig.ImapLogfile = "";
                defaultConfig.Smtp = new ConnectionData()
                {
                    Host = "smtp.example.com",
                    Port = 465,
                    User = "user",
                    Password = "password"
                };
                defaultConfig.SmtpLogfile = "";
                defaultConfig.AcceptSslCertificateFromDomain = "example.com";
                defaultConfig.AcceptSslCertificateWithHash = "";
                defaultConfig.MailFromName = "Sendungsverfolgung von XYZ";
                defaultConfig.MailFromAddress = "mail@example.com";
                defaultConfig.WhiteListedMailAddresses = new List<string> { "info@paket.dpd.de", "pickup@paketshop.dpd.de" };

                File.WriteAllLines(
                    configfile, 
                    JsonConvert.SerializeObject(defaultConfig, Formatting.Indented)
                        .Split(new[] { Environment.NewLine },StringSplitOptions.None));
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not create default config file: {configfile}. Reason: {ex}");
                throw;
            }
        }
    }
}
