﻿namespace Xantiva.TrackingMailProvider.Shared.Configuration
{
    public interface IConnectionData
    {
        string Host { get; set; }
        string Password { get; set; }
        int Port { get; set; }
        string User { get; set; }
    }
}