﻿using System.Collections.Generic;

namespace Xantiva.TrackingMailProvider.Shared.Configuration
{
    /// <summary>
    /// Interface or the configuration parameter data.
    /// </summary>
    public interface IConfigParams
    {
        /// <summary>
        /// <see cref="ConnectionData"/> for the IMAP server.
        /// </summary>
        ConnectionData Imap { get; set; }

        /// <summary>
        /// Filepath of the log file of the IMAP client. Leave filepath emtpy, if no logging is required.
        /// </summary>
        string ImapLogfile { get; set; }

        /// <summary>
        /// <see cref="ConnectionData"/> for the SMTP server.
        /// </summary>
        ConnectionData Smtp { get; set; }

        /// <summary>
        /// Filepath of the log file of the SMTP client. Leave filepath emtpy, if no logging is required.
        /// </summary>
        string SmtpLogfile { get; set; }

        /// <summary>
        /// If the domain name (e.g. example.com) is set, the validator will only accept certificates without errors from this domain.
        /// </summary>
        string AcceptSslCertificateFromDomain { get; set; }

        /// <summary>
        /// If the hash is set, the validator will only accept certificates with this hash.
        /// </summary>
        string AcceptSslCertificateWithHash { get; set; }

        /// <summary>
        /// Accepts valid certificates with a name mismatch.
        /// </summary>
        bool AcceptRemoteCertificateNameMismatch { get; set; }

        /// <summary>
        /// The 'from' mail address.
        /// </summary>
        string MailFromAddress { get; set; }

        /// <summary>
        /// The 'from' name.
        /// </summary>
        string MailFromName { get; set; }

        /// <summary>
        /// A collection of mail addresses as "white list".
        /// </summary>
        ICollection<string> WhiteListedMailAddresses { get; set; }
    }
}