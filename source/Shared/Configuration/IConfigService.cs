﻿namespace Xantiva.TrackingMailProvider.Shared.Configuration
{
    /// <summary>
    /// Interface for the configuration service which supports the <see cref="IConfigParams"/> and the folders.
    /// </summary>
    public interface IConfigService
    {
        /// <summary>
        /// Path to the folder from where the mail addresses should be imported.
        /// </summary>
        string Importfolder { get; }

        /// <summary>
        /// Path to the folder from where the application should "work" in. Contains the import, database folder, ...
        /// </summary>
        string Workfolder { get; }

        /// <summary>
        /// The <see cref="IConfigParams"/> configuration data.
        /// </summary>
        IConfigParams ConfigParams { get; }
    }
}