﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xantiva.TrackingMailProvider.Shared.Configuration
{
    public class ConnectionData : IConnectionData
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}
