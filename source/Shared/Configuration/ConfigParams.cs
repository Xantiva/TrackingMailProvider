﻿using System.Collections.Generic;

namespace Xantiva.TrackingMailProvider.Shared.Configuration
{
    /// <summary>
    /// The configuration parameter data.
    /// </summary>
    public class ConfigParams : IConfigParams
    {
        /// <summary>
        /// <see cref="ConnectionData"/> for the IMAP server.
        /// </summary>
        public ConnectionData Imap { get; set; }

        /// <summary>
        /// Filepath of the log file of the IMAP client. Leave filepath emtpy, if no logging is required.
        /// </summary>
        public string ImapLogfile {get; set;}

        /// <summary>
        /// <see cref="ConnectionData"/> for the SMTP server.
        /// </summary>
        public ConnectionData Smtp { get; set; }

        /// <summary>
        /// Filepath of the log file of the SMTP client. Leave filepath emtpy, if no logging is required.
        /// </summary>
        public string SmtpLogfile { get; set; }

        /// <summary>
        /// If the domain name (e.g. example.com) is set, the validator will only accept certificates without errors from this domain.
        /// </summary>
        public string AcceptSslCertificateFromDomain { get; set; }

        /// <summary>
        /// If the hash is set, the validator will only accept certificates with this hash.
        /// </summary>
        public string AcceptSslCertificateWithHash { get; set; }

        /// <summary>
        /// Accepts valid certificates with a name mismatch.
        /// </summary>
        public bool AcceptRemoteCertificateNameMismatch { get; set; }

        /// <summary>
        /// The 'from' mail address.
        /// </summary>
        public string MailFromAddress { get; set; }
        
        /// <summary>
        /// The 'from' name.
        /// </summary>
        public string MailFromName { get; set; }

        /// <summary>
        /// A collection of mail addresses as "white list".
        /// </summary>
        public ICollection<string> WhiteListedMailAddresses { get; set; }
    }

}
