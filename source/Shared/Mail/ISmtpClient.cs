﻿using MimeKit;

namespace Xantiva.TrackingMailProvider.Shared.Mail
{
    public interface ISmtpClient
    {
        void Connect(string host, int port, string user, string password);
        void Dispose();
        void ForwardMail(MimeMessage message, MailboxAddress customerMailAddress);
    }
}