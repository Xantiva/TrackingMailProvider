﻿using System;
using System.Diagnostics;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Xantiva.TrackingMailProvider.Shared.Configuration;

namespace Xantiva.TrackingMailProvider.Shared.Mail
{
    /// <summary>
    /// Validator for the server certificate
    /// </summary>
    public class ServerCertificateValidator : IServerCertificateValidator
    {
        protected IConfigParams ConfigParams { get; }

        /// <summary>
        /// Creates an <see cref="ServerCertificateValidator"/> instance.
        /// </summary>
        /// <param name="configService"></param>
        public ServerCertificateValidator(IConfigService configService)
        {
            ConfigParams = configService?.ConfigParams ?? throw new ArgumentNullException(nameof(configService), "The config service must not be NULL.");
        }

        /// <summary>
        /// Validates the certifate of the server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cert">The <see cref="X509Certificate"/> from the server.</param>
        /// <param name="chain">The certificate <see cref="X509Chain"/></param>.
        /// <param name="error">The <see cref="SslPolicyErrors"/>.</param>
        /// <returns><c>true</c>, if the certificate is valid. <c>false</c>, if the certificate isn't valid.</returns>
        public bool Validate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error)
        {
            Trace.TraceInformation($"Host {sender}, Certificate {cert.Subject}");
            Trace.TraceInformation($"Certificate hash {cert.GetCertHashString()}");
            Trace.TraceInformation($"Ssl policy errors {error}");
            if (!string.IsNullOrWhiteSpace(ConfigParams.AcceptSslCertificateWithHash))
            {
                if (ConfigParams.AcceptSslCertificateWithHash == cert.GetCertHashString())
                {
                    Trace.TraceInformation("Certificate valid. (Hash string equals AcceptSslCertificateWithHash.)");
                    return true;
                }
                Trace.TraceWarning("Certificate NOT valid. (Hash string differs from AcceptSslCertificateWithHash.)");
                return false;
            }
            else if (!string.IsNullOrWhiteSpace(ConfigParams.AcceptSslCertificateFromDomain))
            {
                if (sender.ToString() == ConfigParams.AcceptSslCertificateFromDomain)
                {
                    if (error == SslPolicyErrors.None)
                    {
                        Trace.TraceInformation("Certificate valid. (Valid certificate for host name.)");
                        return true;
                    }
                    else if (error == SslPolicyErrors.RemoteCertificateNameMismatch)
                    {
                        Trace.TraceWarning("Certificate valid. (Certificate matches the host name and has a 'RemoteCertificateNameMismatch' error, but this is allowed.)");
                        return true;
                    }
                    Trace.TraceWarning("Certificate NOT valid. (Certificate matches the host name, but has errors.)");
                    return false;
                }
                Trace.TraceWarning("Certificate NOT valid. (Host differs from AcceptSslCertificateFromDomain.)");
                return false;
            }

            Trace.TraceWarning("Could not validate certificate because no method selected. At least one method has to be configured: AcceptSslCertificateWithHash or AcceptSslCertificateFromDomain.");
            return false;
        }
    }
}
