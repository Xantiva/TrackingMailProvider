﻿using MailKit;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xantiva.TrackingMailProvider.Shared.Mail
{
    public interface IMailEnvelope
    {
        MimeMessage Message { get; }
        UniqueId Id { get; }
    }
}
