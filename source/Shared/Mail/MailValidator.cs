﻿using MimeKit;
using System;
using System.Diagnostics;
using Xantiva.TrackingMailProvider.Shared.Configuration;

namespace Xantiva.TrackingMailProvider.Shared.Mail
{
    public class MailValidator : IMailValidator
    {
        protected IConfigParams ConfigParams { get; }

        public MailValidator(IConfigService configService)
        {
            ConfigParams = configService?.ConfigParams ?? throw new ArgumentNullException(nameof(configService), "The config service and params must not be NULL.");
        }

        public bool IsValid(MimeMessage message)
        {
            if (!IsWhiteListedSender(message)) return false;
            if (!HasValidSpfRecord(message)) return false;

            Trace.TraceInformation("Validation passed.");
            return true;
        }

        private bool HasValidSpfRecord(MimeMessage message)
        {
            const string spfField = "Received-SPF";

            if (!message.Headers.Contains(spfField))
            {
                Trace.TraceWarning("The message header doesn't contain a SPF record.");
                return false;
            }

            var spfHeader = message.Headers[message.Headers.IndexOf(spfField)];
            if (spfHeader.Value.StartsWith("pass")) return true;

            Trace.TraceWarning("The SPF record is not 'pass'.");
            return false;
        }

        private bool IsWhiteListedSender(MimeMessage message)
        {
            if (!(message.From[0] is MailboxAddress fromAddress)) throw new ArgumentOutOfRangeException(nameof(message), message, "The to-addresses have to be of type MailboxAddress.");

            if (ConfigParams.WhiteListedMailAddresses.Contains(fromAddress.Address)) return true;

            Trace.TraceWarning($"The from address is not whitelisted: {fromAddress.Address}");

            return false;
            
        }
    }
}
