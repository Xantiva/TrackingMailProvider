﻿using MimeKit;

namespace Xantiva.TrackingMailProvider.Shared.Mail
{
    public interface IMailValidator
    {
        bool IsValid(MimeMessage message);
    }
}
