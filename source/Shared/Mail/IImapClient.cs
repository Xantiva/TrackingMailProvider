﻿using MailKit;
using System.Collections.Generic;

namespace Xantiva.TrackingMailProvider.Shared.Mail
{
    public interface IImapClient
    {
        void Connect(string host, int port, string user, string password);
        void Dispose();
        IEnumerable<MailEnvelope> GetNewValidMails();
        void MoveMailToDone(UniqueId uniqueId);
        void MoveMailToUnkown(UniqueId uniqueId);
    }
}