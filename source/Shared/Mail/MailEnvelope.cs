﻿using System;
using MailKit;
using MimeKit;

namespace Xantiva.TrackingMailProvider.Shared.Mail
{
    public class MailEnvelope : IMailEnvelope
    {
        public MimeMessage Message { get; }

        public UniqueId Id { get; }

        public MailEnvelope(MimeMessage message, UniqueId id)
        {
            Message = message ?? throw new ArgumentNullException(nameof(message), "The message must not be NULL.");
            Id = id;
        }
    }
}
