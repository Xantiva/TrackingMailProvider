﻿using MailKit;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Utils;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Xantiva.TrackingMailProvider.Shared.Configuration;

namespace Xantiva.TrackingMailProvider.Shared.Mail
{
    public class AppSmtpClient : IDisposable, ISmtpClient
    {
        protected SmtpClient Client { get; }
        protected IConfigParams ConfigParams { get; }
        private IServerCertificateValidator ServerCertificateValidator { get; }

        public AppSmtpClient(IConfigService configService, IServerCertificateValidator serverCertificateValidator)
        {
            ConfigParams = configService?.ConfigParams ?? throw new ArgumentNullException(nameof(configService), "The config service must not be NULL.");
            ServerCertificateValidator = serverCertificateValidator ?? throw new ArgumentNullException(nameof(serverCertificateValidator), "The server certificate validator must not be NULL.");

            var logFile = configService.ConfigParams.SmtpLogfile;
            if (string.IsNullOrWhiteSpace(logFile))
            {
                Trace.TraceInformation("Creating Smtp client without logging ...");
                Client = new SmtpClient();
            }
            else
            {
                var logFolder = Path.GetDirectoryName(logFile);
                if (Directory.Exists(logFolder))
                {
                    Trace.TraceInformation($"Creating Smtp client logging to {logFile} ...");
                    Client = new SmtpClient(new ProtocolLogger(logFile));
                }
                else
                {
                    Trace.TraceInformation($"Creating Smtp client without logging, because the given path to the log file doesn't exist: {logFile}");
                    Client = new SmtpClient();
                }
            }
        }

        public void Connect(string host, int port, string user, string password)
        {
            try
            {
                Client.ServerCertificateValidationCallback =
                    (sender, cert, chain, error) => ServerCertificateValidator.Validate(sender, cert, chain, error);
                Client.Connect(host, port, true);
                Client.Authenticate(user, password);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not connect to the smtp client. Reason: {ex}");
                throw;
            }
        }

        public void ForwardMail(MimeMessage message, MailboxAddress customerMailAddress)
        {
            try
            {
                var from = new MailboxAddress(ConfigParams.MailFromName, ConfigParams.MailFromAddress);
                message.To.Clear();
                message.To.Add(customerMailAddress);
                message.From.Clear();
                message.From.Add(from);

                // clear the Resent-* headers in case this message has already been Resent...
                message.ResentSender = null;
                message.ResentFrom.Clear();
                message.ResentReplyTo.Clear();
                message.ResentTo.Clear();
                message.ResentCc.Clear();
                message.ResentBcc.Clear();

                // now add our own Resent-* headers...
                message.ResentFrom.Add(from);
                message.ResentReplyTo.Add(customerMailAddress);
                message.ResentTo.Add(customerMailAddress);
                message.ResentMessageId = MimeUtils.GenerateMessageId();
                message.ResentDate = DateTimeOffset.Now;

                // TODO: Zur Kontrolle erst mal als Blindcopy an mich ...
                //message.Bcc.Add(new MailboxAddress("mike@xantiva.de"));
                //message.ResentBcc.Add(new MailboxAddress("mike@xantiva.de"));

                Client.Send(message);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not send the message to {customerMailAddress}. Reason: {ex}");
                throw;
            }
        }



        public void Dispose()
        {
            Trace.TraceInformation("Disposing ...");

            if (Client.IsConnected) Client.Disconnect(true, CancellationToken.None);

            Client.Dispose();
        }
    }
}
