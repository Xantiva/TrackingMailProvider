﻿using MailKit;
using MailKit.Net.Imap;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Xantiva.TrackingMailProvider.Shared.Configuration;

namespace Xantiva.TrackingMailProvider.Shared.Mail
{
    public class AppImapClient : IDisposable, IImapClient
    {
        protected ImapClient Client { get; }
        protected IMailFolder Inbox { get; set; }
        protected IMailFolder UnkownBox { get; set; }
        private const string UnkownBoxName = "unkown";
        protected IMailFolder DoneBox { get; set; }
        private const string DoneBoxName = "done";
        private IMailValidator Validator { get; }
        private IServerCertificateValidator ServerCertificateValidator { get; }


        public AppImapClient(IMailValidator validator, IConfigService configService, IServerCertificateValidator serverCertificateValidator)
        {
            Validator = validator ?? throw new ArgumentNullException(nameof(validator), "The validator must not be NULL.");
            if (configService == null) throw new ArgumentNullException(nameof(configService), "The config service must not be NULL.");
            ServerCertificateValidator = serverCertificateValidator ?? throw new ArgumentNullException(nameof(serverCertificateValidator), "The server certificate validator must not be NULL.");

            var logFile = configService.ConfigParams.ImapLogfile;
            if (string.IsNullOrWhiteSpace(logFile))
            {
                Trace.TraceInformation("Creating Imap client without logging ...");
                Client = new ImapClient();
            }
            else
            {
                var logFolder = Path.GetDirectoryName(logFile);
                if (Directory.Exists(logFolder))
                {
                    Trace.TraceInformation($"Creating Imap client logging to {logFile} ...");
                    Client = new ImapClient(new ProtocolLogger(logFile));
                }
                else
                {
                    Trace.TraceInformation($"Creating Imap client without logging, because the given path to the log file doesn't exist: {logFile}");
                    Client = new ImapClient();
                }
            }
        }


        public void Connect(string host, int port, string user, string password)
        {
            try
            {
                Client.ServerCertificateValidationCallback = 
                    (sender, cert, chain, error) => ServerCertificateValidator.Validate(sender, cert, chain, error);

                Client.Connect(host, port, true);
                Client.Authenticate(user, password);

                // The Inbox folder is always available on all IMAP servers...
                Inbox = Client.Inbox;
                Inbox.Open(FolderAccess.ReadWrite);
                Trace.TraceInformation("Total messages: {0}", Inbox.Count);

                GetFoldersOrCreateIfNecessary(Inbox, CancellationToken.None);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not connect to the imap client. Reason: {ex}");
                throw;
            }
        }

        protected void GetFoldersOrCreateIfNecessary(IMailFolder inbox, CancellationToken cancellationToken)
        {
            try
            {
                try
                {
                    DoneBox = inbox.GetSubfolder(DoneBoxName, cancellationToken);
                }
                catch (FolderNotFoundException)
                {
                    Trace.TraceWarning($"Folder '{DoneBoxName}' could not be found. Trying to create ...");
                    DoneBox = inbox.Create(DoneBoxName, true);
                    Trace.TraceWarning($"Folder '{DoneBoxName}' created.");
                }

                try
                {
                    UnkownBox = inbox.GetSubfolder(UnkownBoxName, cancellationToken);
                }
                catch (FolderNotFoundException)
                {
                    Trace.TraceWarning($"Folder '{UnkownBoxName}' could not be found. Trying to create ...");
                    UnkownBox = inbox.Create(UnkownBoxName, true);
                    Trace.TraceWarning($"Folder '{UnkownBoxName}' created.");
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not connect to or create the folders. Reason: {ex}");
                throw;
            }
        }

        public void Dispose()
        {
            Trace.TraceInformation("Disposing ...");

            if (Client.IsConnected) Client.Disconnect(true, CancellationToken.None);

            Client.Dispose();
        }

        public IEnumerable<MailEnvelope> GetNewValidMails()
        {
            // fetch some useful metadata about each message in the folder...
            var items = Inbox.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.Size | MessageSummaryItems.Flags);

            // iterate over all of the messages and fetch them by UID
            foreach (var item in items)
            {
                var message = Inbox.GetMessage(item.UniqueId);

                // Validierung 
                if (!Validator.IsValid(message))
                {
                    // Do not process, move to unknown folder
                    MoveMailToUnkown(item.UniqueId);
                    continue;
                }

                yield return new MailEnvelope(message, item.UniqueId);
            }

        }

        public void MoveMailToUnkown(UniqueId uniqueId)
        {
            try
            {
                Trace.TraceInformation($"Move mail #{uniqueId} to 'unkown' folder.");
                Inbox.MoveTo(uniqueId, UnkownBox, CancellationToken.None);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not move mail #{uniqueId} to the unkown folder. Reason: {ex}");
                throw;
            }
        }

        public void MoveMailToDone(UniqueId uniqueId)
        {
            try
            {
                Trace.TraceInformation($"Move mail #{uniqueId} to 'done' folder.");
                Inbox.MoveTo(uniqueId, DoneBox, CancellationToken.None);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not move mail #{uniqueId} to the done folder. Reason: {ex}");
                throw;
            }
        }
    }
}
