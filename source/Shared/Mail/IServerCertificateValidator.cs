﻿using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Xantiva.TrackingMailProvider.Shared.Mail
{
    /// <summary>
    /// Interface for a validator for the server certificate
    /// </summary>
    public interface IServerCertificateValidator
    {
        /// <summary>
        /// Validates the certifate of the server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cert">The <see cref="X509Certificate"/> from the server.</param>
        /// <param name="chain">The certificate <see cref="X509Chain"/></param>.
        /// <param name="error">The <see cref="SslPolicyErrors"/>.</param>
        /// <returns><c>true</c>, if the certificate is valid. <c>false</c>, if the certificate isn't valid.</returns>
        bool Validate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error);
    }
}