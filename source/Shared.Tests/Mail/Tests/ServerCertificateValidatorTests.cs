﻿using System;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xantiva.TrackingMailProvider.Shared.Configuration;

namespace Xantiva.TrackingMailProvider.Shared.Mail.Tests
{
    [TestClass]
    public class ServerCertificateValidatorTests
    {
        protected ServerCertificateValidator Sut { get; private set; }

        protected Mock<IConfigService> ConfigServiceMock { get; private set; }
        protected ConfigParams ConfigParams { get; private set; }

        [TestInitialize]
        public void SetupTest()
        {
            ConfigParams = new ConfigParams();
            ConfigServiceMock = new Mock<IConfigService>();
            ConfigServiceMock
                .SetupGet(p => p.ConfigParams)
                .Returns(ConfigParams);

            Sut = new ServerCertificateValidator(ConfigServiceMock.Object);
        }

        [TestMethod]
        public void Constructor()
        {
            Assert.IsInstanceOfType(Sut, typeof(IServerCertificateValidator));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullConfigServiceFails()
        {
            new ServerCertificateValidator(null);
        }

        public TestContext TestContext { get; set; }

        [TestMethod]
        public void Validate_OK_AcceptFromDomain()
        {
            // arrange        
            object sender = "example.com";
            X509Certificate cert = CertificateGenerator.GenerateCertificate("dsfd");
            X509Chain chain = null;
            SslPolicyErrors error = SslPolicyErrors.None;
            ConfigParams.AcceptSslCertificateFromDomain = sender.ToString();

            // act
            var actual = Sut.Validate(sender, cert, chain, error);

            // assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void Validate_OK_AcceptWithHash()
        {
            // arrange        
            object sender = "example.com";
            X509Certificate cert = CertificateGenerator.GenerateCertificate("dsfd");
            X509Chain chain = null;
            SslPolicyErrors error = SslPolicyErrors.None;
            ConfigParams.AcceptSslCertificateWithHash = cert.GetCertHashString();


            // act
            var actual = Sut.Validate(sender, cert, chain, error);

            // assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void Validate_Not_AcceptFromDifferentDomain()
        {
            // arrange        
            object sender = "example.com";
            X509Certificate cert = CertificateGenerator.GenerateCertificate("dsfd");
            X509Chain chain = null;
            SslPolicyErrors error = SslPolicyErrors.None;
            ConfigParams.AcceptSslCertificateFromDomain = "a-different-domain.com";

            // act
            var actual = Sut.Validate(sender, cert, chain, error);

            // assert
            Assert.IsFalse(actual);
        }
    }
}
