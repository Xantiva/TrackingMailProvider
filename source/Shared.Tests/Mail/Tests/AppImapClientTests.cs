﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.IO;
using Xantiva.TrackingMailProvider.Shared.Configuration;

namespace Xantiva.TrackingMailProvider.Shared.Mail.Tests
{
    [TestClass]
    public class AppImapClientTests
    {
        protected AppImapClient Sut { get; private set; }
        protected Mock<IMailValidator> ValidatorMock { get; private set; }
        protected IConfigService TestConfigService { get; private set; }
        protected Mock<IServerCertificateValidator> ServerCertificateValidatorMock { get; private set; }

        [TestInitialize]
        public void SetTestUp()
        {
            var workfolder = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                "Xantiva",
                "TrackingMailProvider", "Test");
            TestConfigService = new ConfigService(workfolder);
            ValidatorMock = new Mock<IMailValidator>();
            ServerCertificateValidatorMock = new Mock<IServerCertificateValidator>();

            Sut = new AppImapClient(ValidatorMock.Object, TestConfigService, ServerCertificateValidatorMock.Object);
        }

        [TestCleanup]
        public void CleanupTest()
        {
            Sut?.Dispose();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullValidatorFails()
        {
            new AppImapClient(null, new Mock<IConfigService>().Object, ServerCertificateValidatorMock.Object);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullConfigServiceFails()
        {
            new AppImapClient(ValidatorMock.Object, null, ServerCertificateValidatorMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullServerCertificateValidatorFails()
        {
            new AppImapClient(ValidatorMock.Object, new Mock<IConfigService>().Object, null);
        }

        [TestMethod]
        [TestCategory("needs Mail Account")]
        public void Connect()
        {
            Sut.Dispose();
            var sut = new AppImapClient(ValidatorMock.Object, TestConfigService, new ServerCertificateValidator(TestConfigService));

            var connData = TestConfigService.ConfigParams.Imap;

            sut.Connect(connData.Host, connData.Port, connData.User, connData.Password);

        }
    }
}
