﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MimeKit;
using Moq;
using Xantiva.TrackingMailProvider.Shared.Configuration;

namespace Xantiva.TrackingMailProvider.Shared.Mail.Tests
{
    [TestClass]
    public class AppSmtpClientTests
    {
        protected AppSmtpClient Sut { get; private set; }

        protected IConfigService TestConfigService { get; private set; }
        protected Mock<IServerCertificateValidator> ServerCertificateValidatorMock { get; private set; }


        [TestInitialize]
        public void SetupTest()
        {
            var workfolder = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                    "Xantiva",
                    "TrackingMailProvider", "Test");
            TestConfigService = new ConfigService(workfolder);
            ServerCertificateValidatorMock = new Mock<IServerCertificateValidator>();

            Sut = new AppSmtpClient(TestConfigService, ServerCertificateValidatorMock.Object);
        }

        [TestCleanup]
        public void CleanupTest()
        {
            Sut?.Dispose();
        }


        [TestMethod]
        public void Constructor()
        {
            Assert.IsInstanceOfType(Sut, typeof(ISmtpClient));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullConfigServiceFails()
        {
            new AppSmtpClient(null, ServerCertificateValidatorMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullServerCertificateFails()
        {
            new AppSmtpClient(TestConfigService, null);
        }

        [TestMethod]
        [TestCategory("needs Mail Account")]
        public void Connect()
        {
            var connData = TestConfigService.ConfigParams.Smtp;
            Sut.Connect(connData.Host, connData.Port, connData.User, connData.Password);
        }


        [TestMethod]
        [TestCategory("needs Mail Account")]
        public void ForwardMail_ToRealAddress()
        {
            var connData = TestConfigService.ConfigParams.Smtp;

            Sut.Connect(connData.Host, connData.Port, connData.User, connData.Password);
            var message = new MimeMessage(
                new List<MailboxAddress> { new MailboxAddress("tracktest@d9e.de") },
                new List<MailboxAddress> { new MailboxAddress("junk1@xantiva.de") },
                "Forwarded Mail",
                new TextPart("plain")
                {
                    Text = @"Hey Mike,

Klappt das?
-- 
Joey
"
                });

            Sut.ForwardMail(message, new MailboxAddress("mike@xantiva.de"));
        }

        [TestMethod]
        [TestCategory("needs Mail Account")]
        public void ReadAndForwardMail_ToRealAddress()
        {
            var validatorMock = new Mock<IMailValidator>();
            validatorMock.Setup(m => m.IsValid(It.IsAny<MimeMessage>())).Returns(true);
            MimeMessage message = null;
            using (var imap = new AppImapClient(validatorMock.Object, TestConfigService, ServerCertificateValidatorMock.Object))
            {
                var connData = TestConfigService.ConfigParams.Imap;
                imap.Connect(connData.Host, connData.Port, connData.User, connData.Password);
                var newMails = imap.GetNewValidMails();
                message = newMails.ToList().ElementAt(0).Message;

            }
            using (var client = new AppSmtpClient(TestConfigService, ServerCertificateValidatorMock.Object))
            {
                var connData = TestConfigService.ConfigParams.Smtp;
                client.Connect(connData.Host, connData.Port, connData.User, connData.Password);

                client.ForwardMail(message, new MailboxAddress("mike@xantiva.de"));

            }
        }

    }
}
