﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MimeKit;
using Moq;
using Xantiva.TrackingMailProvider.Shared.Configuration;

namespace Xantiva.TrackingMailProvider.Shared.Mail.Tests
{
    [TestClass]
    public class MailValidatorTests
    {

        protected MailValidator Sut { get; private set; }

        protected Mock<IConfigService> ConfigServiceMock { get; private set; }
        protected IConfigParams ConfigParams { get; private set; }
        protected string ValidMailAddress => "valid@example.com";
        protected Header SpfPassHeader => new Header("Received-SPF", "pass (lvps46-163-115-211.dedicated.hosteurope.de: domain of tr.inxmail-commerce.com designates 93.191.162.37 as permitted sender) client-ip=93.191.162.37; envelope-from=bounce_dpdde+mll6xnndmiwixljffdee3k2wtaq@tr.inxmail-commerce.com; helo=inxmx87.inxserver.de;");

        [TestInitialize]
        public void SetupTest()
        {
            ConfigServiceMock = new Mock<IConfigService>();
            ConfigParams = new ConfigParams();
            ConfigParams.WhiteListedMailAddresses = new List<string> { ValidMailAddress };
            ConfigServiceMock
                .SetupGet(p => p.ConfigParams)
                .Returns(ConfigParams);

            Sut = new MailValidator(ConfigServiceMock.Object);
        }

        [TestMethod]
        public void Constructor()
        {
            Assert.IsInstanceOfType(Sut, typeof(IMailValidator));
        }


        [TestMethod]
        public void IsValid_Yes()
        {
            // arrange
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(ValidMailAddress));
            message.Headers.Add(SpfPassHeader);

            // act
            var result = Sut.IsValid(message);

            // assert
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void IsValid_NotWhiteListedSender()
        {
            // arrange
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("mail@example.com"));
            message.Headers.Add(SpfPassHeader);

            // act
            var result = Sut.IsValid(message);

            // assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsValid_NoSpfPass()
        {
            // Received - SPF: none(lvps46 - 163 - 115 - 211.dedicated.hosteurope.de: no valid SPF record)

            // arrange
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(ValidMailAddress));
            message.Headers.Add("Received-SPF", "none (lvps46-163-115-211.dedicated.hosteurope.de: no valid SPF record)");

            // act
            var result = Sut.IsValid(message);

            // assert
            Assert.IsFalse(result);
        }

    }
}
