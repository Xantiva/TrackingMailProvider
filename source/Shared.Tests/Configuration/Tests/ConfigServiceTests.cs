﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Xantiva.TrackingMailProvider.Shared.Configuration.Tests
{
    [TestClass]
    public class ConfigServiceTests
    {

        public TestContext TestContext { get; set; }

        [TestMethod]
        public void Constuctor_NoConfigFile()
        {
            // arrange
            var workfolder = Path.Combine(TestContext.TestRunDirectory, "noconfigfile");
            Directory.CreateDirectory(workfolder);

            // act
            var sut = new ConfigService(workfolder);

            // assert
            Assert.AreEqual("mail@example.com", sut.ConfigParams.MailFromAddress);

        }
    }
}
