﻿using System.Collections.Generic;
using Xantiva.TrackingMailProvider.Customers.Entities;

namespace Xantiva.TrackingMailProvider.Customers.Persistence
{
    /// <summary>
    /// Interface für das Repository für die Auftragsdaten (<see cref="OrderMailData"/>).
    /// </summary>
    public interface ICustomerMailAddressRepository
    {
        /// <summary>
        /// Löscht Auftragsdaten, die älter als x Tage sind.
        /// </summary>
        /// <param name="days">Wie alt sollen die Auftragsdaten sein, die gelöscht werden?</param>
        /// <returns>Die Anzahl der Einträge, die gelöscht wurden.</returns>
        int DeleteItemsOlderThan(int days);

        /// <summary>
        /// Holt das <see cref="OrderMailData"/>-Objekt zu der entsprechenden Mailadresse mit der Auftragsnummer aus der DB.
        /// </summary>
        /// <param name="orderMailAddress">Die Mailadresse mit der Auftragsnummer, nach der gesucht werden soll.</param>
        /// <returns><c>NULL</c> wenn nichts gefunden wurde, andernfalls das <see cref="OrderMailData"/>-Objekt.</returns>
        OrderMailData GetDataByOrderMailAddress(string orderMailAddress);

        /// <summary>
        /// Fügt ein neues <see cref="OrderMailData"/>-Objekt ein.
        /// </summary>
        /// <param name="orderMailData">Das <see cref="OrderMailData"/>-Objekt, was eingefügt werden soll.</param>
        /// <exception cref="LiteDB.LiteException">Wenn ein Eintrag mit bereits bestehende Id hinzugefügt werden soll.</exception>
        void Insert(OrderMailData orderMailData);

        /// <summary>
        /// Fügt mehrere <see cref="OrderMailData"/>-Objekte auf einmal in die Datenbank.
        /// </summary>
        /// <param name="orderMailDatas">Die <see cref="IEnumerable{T}"/> Auflistung der Objekte.</param>
        /// <exception cref="LiteDB.LiteException">Wenn ein Eintrag mit bereits bestehende Id hinzugefügt werden soll.</exception>
        void InsertBulk(IEnumerable<OrderMailData> orderMailDatas);
    }
}