﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xantiva.TrackingMailProvider.Customers.Entities;

namespace Xantiva.TrackingMailProvider.Customers.Persistence
{
    /// <summary>
    /// Repository für die Auftragsdaten und Mailadressen der Kunden.
    /// </summary>
    public class CustomerMailAddressRepository : ICustomerMailAddressRepository
    {
        private readonly string _connectionString;


        /// <summary>
        /// Erzeugt eine neue <see cref="CustomerMailAddressRepository"/> Instanz.
        /// </summary>
        /// <param name="connectionString">Der Connectionstring für die LiteDB-Datenbank.
        /// Ohne besondere weitere Parameter reicht ein "Filename={filename}" aus.</param>
        public CustomerMailAddressRepository(string connectionString)
        {
            Trace.TraceInformation($"Creating CustomerMailAddressRepository with connection string: {connectionString}");

            if (string.IsNullOrWhiteSpace(connectionString)) throw new ArgumentNullException(nameof(connectionString), "The connection string must not be NULL or empty.");

            _connectionString = connectionString;
        }

        /// <summary>
        /// Holt das <see cref="OrderMailData"/>-Objekt zu der entsprechenden Mailadresse mit der Auftragsnummer aus der DB.
        /// </summary>
        /// <param name="orderMailAddress">Die Mailadresse mit der Auftragsnummer, nach der gesucht werden soll.</param>
        /// <returns><c>NULL</c> wenn nichts gefunden wurde, andernfalls das <see cref="OrderMailData"/>-Objekt.</returns>
        public OrderMailData GetDataByOrderMailAddress(string orderMailAddress)
        {
            try
            {
                var lowerOrderMailAddress = orderMailAddress.ToLower();
                using (var db = new LiteRepository(_connectionString))
                {
                    var result = db.SingleOrDefault<OrderMailData>(x => x.OrderMailAddress == lowerOrderMailAddress);
                    Trace.TraceInformation($"GetDataByOrderMailAddress {orderMailAddress}: {result}");
                    return result;
                }                
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not get data by order mail address {orderMailAddress}. Reason: {ex}");
                throw;
            }
            
        }

        /// <summary>
        /// Fügt ein neues <see cref="OrderMailData"/>-Objekt ein.
        /// </summary>
        /// <param name="orderMailData">Das <see cref="OrderMailData"/>-Objekt, was eingefügt werden soll.</param>
        /// <exception cref="LiteDB.LiteException">Wenn ein Eintrag mit bereits bestehende Id hinzugefügt werden soll.</exception>
        public void Insert(OrderMailData orderMailData)
        {
            if (orderMailData == null) throw new ArgumentNullException(nameof(orderMailData), "The order mail data must not be NULL.");

            try
            {
                using (var db = new LiteRepository(_connectionString))
                {
                    Trace.TraceInformation($"Insert data from order {orderMailData.Id}.");
                    db.Insert(orderMailData);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not insert data {orderMailData}, reason: {ex}");
                throw;
            }
        }

        /// <summary>
        /// Fügt mehrere <see cref="OrderMailData"/>-Objekte auf einmal in die Datenbank.
        /// </summary>
        /// <param name="orderMailDatas">Die <see cref="IEnumerable{T}"/> Auflistung der Objekte.</param>
        /// <exception cref="LiteDB.LiteException">Wenn ein Eintrag mit bereits bestehende Id hinzugefügt werden soll.</exception>
        public void InsertBulk(IEnumerable<OrderMailData> orderMailDatas)
        {
            if (orderMailDatas == null) throw new ArgumentNullException(nameof(orderMailDatas), "The order mail data must not be NULL.");

            try
            {
                using (var db = new LiteRepository(_connectionString))
                {
                    Trace.TraceInformation($"Insert bulk data ...");
                    foreach (var orderMailData in orderMailDatas)
                    {
                        var result = db.SingleOrDefault<OrderMailData>(x => x.Id == orderMailData.Id);
                        if (result != null) continue; // Item exists
                        Trace.TraceInformation($"Insert data from order {orderMailData.Id}.");
                        db.Insert(orderMailData);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not insert bulk data, reason: {ex}");
                throw;
            }
        }

        /// <summary>
        /// Löscht Auftragsdaten, die älter als x Tage sind.
        /// </summary>
        /// <param name="days">Wie alt sollen die Auftragsdaten sein, die gelöscht werden?</param>
        /// <returns>Die Anzahl der Einträge, die gelöscht wurden.</returns>
        public int DeleteItemsOlderThan(int days)
        {
            if (days < 1) throw new ArgumentOutOfRangeException(nameof(days),days, "The days must be positive.");

            var deleted = 0;

            try
            {
                using (var db = new LiteRepository(_connectionString))
                {
                    deleted = db.Delete<OrderMailData>(x => x.OrderDate < DateTime.Now.AddDays(-days));
                    Trace.TraceInformation($"Deleted {deleted} item, which where older than {days} days.");
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not delete older data, reason: {ex}");
                throw;
            }

            return deleted;
        }
    }
}
