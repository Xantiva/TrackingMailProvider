﻿using CsvHelper;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Xantiva.TrackingMailProvider.Customers.Entities;

namespace Xantiva.TrackingMailProvider.Customers.Import
{
    /// <summary>
    /// Importer für JTL-Ameise-Dateien.
    /// </summary>
    public class JtlOrderDataImporter : IOrderDataImporter
    {
        /// <summary>
        /// Importiert die Auftragsdaten aus einer Datei der JTL-Ameise.
        /// </summary>
        /// <param name="filename">Pfad zur Datei.</param>
        /// <returns>Die Auflistung der importierten <see cref="OrderMailData"/>.</returns>
        public IEnumerable<OrderMailData> Import(string filename)
        {
            Trace.TraceInformation($"Importing from {filename} ...");

            using (var reader = new StreamReader(filename))
            {
                var csv = new CsvReader(reader);
                csv.Configuration.RegisterClassMap<JtlOrderMailDataMap>();
                csv.Configuration.Delimiter = ";";
                csv.Configuration.Quote = '"';
                //csv.Read();
                //csv.ReadHeader();
                csv.Configuration.HasHeaderRecord = false;
                var returnedIds = new HashSet<string>();
                var count = 0;
                var imported = 0;
                while (csv.Read())
                {
                    count++;
                    var record = csv.GetRecord<OrderMailData>();
                    if (!returnedIds.Contains(record.Id) && !string.IsNullOrEmpty(record.CustomerMailAddress))
                    {
                        returnedIds.Add(record.Id);
                        imported++;
                        yield return record;
                    }
                }

                Trace.TraceInformation($"Entries in file: {count}, imported: {imported}");
            }
        }
    }
}

