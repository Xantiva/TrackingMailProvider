﻿using System.Collections.Generic;
using Xantiva.TrackingMailProvider.Customers.Entities;

namespace Xantiva.TrackingMailProvider.Customers.Import
{
    public interface IOrderDataImporter
    {
        /// <summary>
        /// Importiert die Auftragsdaten aus einer Datei.
        /// </summary>
        /// <param name="filename">Pfad zur Datei.</param>
        /// <returns>Die Auflistung der importierten <see cref="OrderMailData"/>.</returns>
        IEnumerable<OrderMailData> Import(string filename);
    }
}
