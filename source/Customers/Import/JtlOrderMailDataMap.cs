﻿using CsvHelper.Configuration;
using Xantiva.TrackingMailProvider.Customers.Entities;

namespace Xantiva.TrackingMailProvider.Customers.Import
{
    internal sealed class JtlOrderMailDataMap : ClassMap<OrderMailData>
    {
        internal JtlOrderMailDataMap()
        {
            Map(m => m.Id).Index(0);
            Map(m => m.OrderMailAddress).Index(1);
            Map(m => m.CustomerMailAddress).Index(2);
            Map(m => m.OrderDate).Index(3);
        }
    }
}
