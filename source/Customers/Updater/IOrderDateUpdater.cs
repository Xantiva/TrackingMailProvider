﻿namespace Xantiva.TrackingMailProvider.Customers.Updater
{
    public interface IOrderDateUpdater
    {
        /// <summary>
        /// Aktualisiert das Repository in dem alle Dateien aus dem Import-Verzeichnis eingelesen werden.
        /// </summary>
        void UpdateRepository();
    }
}