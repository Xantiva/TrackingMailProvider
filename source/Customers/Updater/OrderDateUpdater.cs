﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Xantiva.TrackingMailProvider.Customers.Import;
using Xantiva.TrackingMailProvider.Customers.Persistence;
using Xantiva.TrackingMailProvider.Shared.Configuration;

namespace Xantiva.TrackingMailProvider.Customers.Updater
{
    public class OrderDateUpdater : IOrderDateUpdater
    {
        protected IConfigService ConfigService { get; }
        protected IOrderDataImporter Importer { get;  }
        protected ICustomerMailAddressRepository Repository { get; }

        public OrderDateUpdater(IConfigService configService, IOrderDataImporter importer, ICustomerMailAddressRepository repository)
        {
            ConfigService = configService ?? throw new ArgumentNullException(nameof(configService), "The configService must not be NULL.");
            Importer = importer ?? throw new ArgumentNullException(nameof(importer), "The importer must not be NULL.");
            Repository = repository ?? throw new ArgumentNullException(nameof(repository), "The repository must not be NULL.");
        }

        public void UpdateRepository()
        {
            if (!Directory.Exists(ConfigService.Importfolder)) throw new DirectoryNotFoundException($"The directory '{ConfigService.Importfolder}' does not exist.");

            Trace.TraceInformation("Updating the customer mail address repository ...");

            var dataFiles = Directory.GetFiles(ConfigService.Importfolder);

            foreach (var file in dataFiles)
            {
                try
                {
                    Trace.TraceInformation($"Processing file {file} ...");
                    var data = Importer.Import(file);
                    Repository.InsertBulk(data);
                    File.Delete(file);
                    Trace.TraceInformation($"Data imported and file deleted.");
                }
                catch (Exception ex)
                {
                    Trace.TraceError($"Failed to import customer order data from file {file}, reason: {ex}");
                    throw;
                }
            }
        }
    }
}
