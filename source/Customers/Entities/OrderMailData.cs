﻿using System;

namespace Xantiva.TrackingMailProvider.Customers.Entities
{
    /// <summary>
    /// Datenklasse für die Datenbank
    /// </summary>
    public class OrderMailData
    {
        /// <summary>
        /// Die Auftragsnummer.
        /// </summary>
        /// <remarks>Für LiteDb brauche ich ein Feld mit Namen Id.
        /// Wenn ich das Feld OrderNumber benenne und dann per BsonId-Attribut auszeichne,
        /// dann funktioniert eine Suche danach nicht:
        /// col.FindOne(Query.EQ(nameof(OrderMailData.OrderNumber), orderNumber));
        /// liefert kein Ergebnis. Ich musste so suchen und dann kann ich das 
        /// col.FindOne(Query.EQ("Id", orderNumber));
        /// Und durch das Attribut hätte die Entity auch eine Abhängigkeit von LiteDb.</remarks>
        public string Id { get; set; }

        private string _orderMailAddress;
        /// <summary>
        /// Die Mailadresse mit der Auftragsnummer.
        /// </summary>
        /// <remarks>Wird nur in Kleinbuchstaben gehalten.</remarks>
        public string OrderMailAddress
        {
            get => _orderMailAddress;
            set => _orderMailAddress = value.ToLower();
        }

        private string _customerMailAddress;
        /// <summary>
        /// Die Mailadresse des Kunden.
        /// </summary>
        /// <remarks>Wird nur in Kleinbuchstaben gehalten.</remarks>
        public string CustomerMailAddress
        {
            get => _customerMailAddress;
            set => _customerMailAddress = value.ToLower();
        }
        /// <summary>
        /// Das Datum des Auftrages.
        /// </summary>
        public DateTime OrderDate { get; set; }
    }
}
