﻿// Verwende die Test-Configuration!
#define USETESTCONFIG

using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xantiva.TrackingMailProvider.Core.Mail;
using Xantiva.TrackingMailProvider.Customers.Import;
using Xantiva.TrackingMailProvider.Customers.Persistence;
using Xantiva.TrackingMailProvider.Customers.Updater;
using Xantiva.TrackingMailProvider.Shared.Configuration;
using Xantiva.TrackingMailProvider.Shared.Mail;

namespace Xantiva.TrackingMailProvider.Core.Bootstrapping.Tests
{
    [TestClass]
    public class BootstrappingTests
    {
        protected Bootstrapping Sut { get; private set; }

        [TestInitialize]
        public void SetTestUp()
        {
            Sut = new Bootstrapping();
        }

        [TestMethod]
        public void ResolveCustomers()
        {
            var container = Sut.Init();

            var connectionString = "a file";
            container.Resolve<ICustomerMailAddressRepository>(
                new NamedParameter(nameof(connectionString), connectionString));
            container.Resolve<IOrderDataImporter>();
            container.Resolve<IOrderDateUpdater>();

        }

        [TestMethod]
        public void ResolveShared()
        {
            var container = Sut.Init();

            container.Resolve<IConfigService>();
            var firstImapClient = container.Resolve<IImapClient>();
            var secondImapClient = container.Resolve<IImapClient>();
            Assert.AreNotSame(firstImapClient, secondImapClient);

            var firstSmtpClient = container.Resolve<ISmtpClient>();
            var secondSmtpClient = container.Resolve<ISmtpClient>();
            Assert.AreNotSame(firstSmtpClient, secondSmtpClient);
            container.Resolve<IMailValidator>();
            container.Resolve<IServerCertificateValidator>();
        }

        [TestMethod]
        public void ResolveCore()
        {
            var container = Sut.Init();

            container.Resolve<IMailProcessor>();

        }
    }
}
