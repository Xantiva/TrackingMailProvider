﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MimeKit;
using Moq;
using Xantiva.TrackingMailProvider.Customers.Entities;
using Xantiva.TrackingMailProvider.Customers.Persistence;
using Xantiva.TrackingMailProvider.Shared.Configuration;
using Xantiva.TrackingMailProvider.Shared.Mail;

namespace Xantiva.TrackingMailProvider.Core.Mail.Tests
{
    [TestClass]
    public class MailProcessorTests
    {
        protected MailProcessor Sut { get; set; }

        protected Mock<IConfigService> ConfigServiceMock { get; private set; }
        protected Mock<IConfigParams> ConfigParamsMock { get; private set; }
        protected Mock<ICustomerMailAddressRepository> CustomerRepositoryMock { get; private set; }
        protected Mock<IImapClient> ImapClientMock { get; private set; }
        protected Mock<ISmtpClient> SmtpClientMock { get; private set; }

        [TestInitialize]
        public void SetupTest()
        {
            ConfigParamsMock = new Mock<IConfigParams>();
            ConfigServiceMock = new Mock<IConfigService>();
            ConfigServiceMock
                .SetupGet(p => p.ConfigParams)
                .Returns(ConfigParamsMock.Object);
            CustomerRepositoryMock = new Mock<ICustomerMailAddressRepository>();
            ImapClientMock = new Mock<IImapClient>();
            SmtpClientMock = new Mock<ISmtpClient>();

            Sut = new MailProcessor(
                ConfigServiceMock.Object,
                CustomerRepositoryMock.Object,
                ImapClientMock.Object,
                SmtpClientMock.Object);
        }

        #region Constructor

        [TestMethod]
        public void Constructor()
        {
            Assert.IsInstanceOfType(Sut, typeof(IMailProcessor));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorNullConfigServiceFails()
        {
            var sut = new MailProcessor(
                null,
                CustomerRepositoryMock.Object,
                ImapClientMock.Object,
                SmtpClientMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorNullCustomerRepositoryFails()
        {
            var sut = new MailProcessor(
                ConfigServiceMock.Object,
                null,
                ImapClientMock.Object,
                SmtpClientMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorNullImapClientFails()
        {
            var sut = new MailProcessor(
                ConfigServiceMock.Object,
                CustomerRepositoryMock.Object,
                null,
                SmtpClientMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorNullSmtpClientFails()
        {
            var sut = new MailProcessor(
                ConfigServiceMock.Object,
                CustomerRepositoryMock.Object,
                ImapClientMock.Object,
                null);
        }

        #endregion


        [TestMethod]
        public void ProcessMails()
        {
            // arrange
            var connData = new ConnectionData()
            {
                Host = "mail.example.com",
                Port = 123,
                User = "user",
                Password = "pw123"
            };
            ConfigParamsMock
                .SetupGet(p => p.Imap)
                .Returns(connData);
            ConfigParamsMock
                .SetupGet(p => p.Smtp)
                .Returns(connData);
            const string customerMailAddress = "mike@xantiva.de";
            const string trackingMailAddress = "AU-123@example.com";
            var message = new MimeMessage();
            message.To.Add(new MailboxAddress(trackingMailAddress));
            var uId = new MailKit.UniqueId(854);
            var mailEnvelope = new MailEnvelope(message, uId);
            ImapClientMock
                .Setup(m => m.GetNewValidMails())
                .Returns(new List<MailEnvelope> { mailEnvelope });
            CustomerRepositoryMock
                .Setup(m => m.GetDataByOrderMailAddress(trackingMailAddress))
                .Returns(
                    new OrderMailData
                    {
                        CustomerMailAddress = customerMailAddress,
                        OrderMailAddress = trackingMailAddress
                    });
            string forwardedAddress = null;
            SmtpClientMock
                .Setup(m => m.ForwardMail(It.IsAny<MimeMessage>(), It.IsAny<MailboxAddress>()))
                .Callback((MimeMessage m, MailboxAddress a) =>
                {
                    forwardedAddress = a.Address;
                });

            // act
            Sut.ProcessMails();

            // assert
            ImapClientMock
                .Verify(m => m.Connect(connData.Host, connData.Port, connData.User, connData.Password), Times.Once);
            SmtpClientMock
                .Verify(m => m.Connect(connData.Host, connData.Port, connData.User, connData.Password), Times.Once);
            SmtpClientMock
                .Verify(m => m.ForwardMail(message, It.IsAny<MailboxAddress>()), Times.Once);
            Assert.AreEqual(customerMailAddress, forwardedAddress);
            ImapClientMock
                .Verify(m => m.MoveMailToDone(uId), Times.Once);

        }

    }
}
