﻿using Autofac;
using System;
using System.Diagnostics;
using Xantiva.TrackingMailProvider.Core.Bootstrapping;
using Xantiva.TrackingMailProvider.Core.Logging;
using Xantiva.TrackingMailProvider.Core.Mail;
using Xantiva.TrackingMailProvider.Customers.Updater;

namespace Xantiva.TrackingMailProvider.Shell
{
    class Program
    {
        static void Main(string[] args)
        {
            var logger = new NLogTraceListenerWrapper();

            Trace.TraceInformation("Starting TrackingMailProvider ...");
            Console.WriteLine("Starting TrackingMailProvider ...");

            var bootstrapping = new Bootstrapping();
            var container = bootstrapping.Init();

            using (var scope = container.BeginLifetimeScope())
            {
                var updater = scope.Resolve<IOrderDateUpdater>();
                updater.UpdateRepository();
                var mailProcessor = scope.Resolve<IMailProcessor>();
                mailProcessor.ProcessMails();
            }

            Trace.TraceInformation("### TrackingMailProvider finished.");
            Trace.TraceInformation("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            Console.WriteLine("TrackingMailProvider finished.");
        }
    }
}
