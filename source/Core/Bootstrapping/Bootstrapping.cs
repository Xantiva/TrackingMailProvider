﻿using Autofac;
using System;
using System.Diagnostics;
using System.IO;
using Xantiva.TrackingMailProvider.Core.Mail;
using Xantiva.TrackingMailProvider.Customers.Import;
using Xantiva.TrackingMailProvider.Customers.Persistence;
using Xantiva.TrackingMailProvider.Customers.Updater;
using Xantiva.TrackingMailProvider.Shared.Configuration;
using Xantiva.TrackingMailProvider.Shared.Mail;

namespace Xantiva.TrackingMailProvider.Core.Bootstrapping
{
    public class Bootstrapping
    {
        public IContainer Init()
        {
            IContainer container = null;
            try
            {
                Trace.TraceInformation("Init Bootstrapping ...");

                var workfolder = Path.Combine(
                         Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                         "Xantiva",
                         "TrackingMailProvider");

                var builder = new ContainerBuilder();

                RegisterShared(builder, workfolder);
                RegisterCustomers(builder, workfolder);
                RegisterCore(builder);

                container = builder.Build();

                Trace.TraceInformation("Bootstrapping completed.");
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Could not complete bootstrapping. Reason: {ex}");
                throw;
            }

            return container;
        }

        private static void RegisterShared(ContainerBuilder builder, string workfolder)
        {

            builder.RegisterType<ConfigService>()
                    .As<IConfigService>()
                    .WithParameter(nameof(workfolder), workfolder)
                    .SingleInstance();

            builder.RegisterType<AppImapClient>()
                    .As<IImapClient>()
                    .InstancePerDependency();

            builder.RegisterType<AppSmtpClient>()
                    .As<ISmtpClient>()
                    .InstancePerDependency();

            builder.RegisterType<MailValidator>()
                    .As<IMailValidator>()
                    .SingleInstance();

            builder.RegisterType<ServerCertificateValidator>()
                    .As<IServerCertificateValidator>()
                    .SingleInstance();
        }

        private static void RegisterCustomers(ContainerBuilder builder, string workfolder)
        {
            var dbFolder = Path.Combine(workfolder, "DB");
            if (!Directory.Exists(dbFolder)) Directory.CreateDirectory(dbFolder);
            var filename = Path.Combine(dbFolder, "customerData.db");
            var connectionString = $"Filename={filename}";

            builder.RegisterType<CustomerMailAddressRepository>()
                    .As<ICustomerMailAddressRepository>()
                    .WithParameter(nameof(connectionString), connectionString)
                    .SingleInstance();

            builder.RegisterType<JtlOrderDataImporter>()
                    .As<IOrderDataImporter>()
                    .SingleInstance();

            builder.RegisterType<OrderDateUpdater>()
                    .As<IOrderDateUpdater>()
                    .SingleInstance();
        }


        private static void RegisterCore(ContainerBuilder builder)
        {
            builder.RegisterType<MailProcessor>()
                    .As<IMailProcessor>()
                    .SingleInstance();
        }

    }
}
