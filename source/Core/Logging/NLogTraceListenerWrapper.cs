﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Xantiva.TrackingMailProvider.Core.Logging
{
    public class NLogTraceListenerWrapper
    {
        public NLogTraceListenerWrapper()
        {
            Trace.Listeners.Add(new NLogTraceListener());
            Trace.TraceInformation("#################################################################");
            Trace.TraceInformation("### Starting TrackingMailProvider ...");
            Trace.Flush();
        }
    }
}
