﻿using MimeKit;
using System;
using System.Diagnostics;
using Xantiva.TrackingMailProvider.Customers.Persistence;
using Xantiva.TrackingMailProvider.Shared.Configuration;
using Xantiva.TrackingMailProvider.Shared.Mail;

namespace Xantiva.TrackingMailProvider.Core.Mail
{
    public class MailProcessor : IMailProcessor
    {
        protected IConfigParams ConfigParams { get; }
        protected ICustomerMailAddressRepository CustomerRepository { get; }
        protected IImapClient ImapClient { get; }
        protected ISmtpClient SmtpClient { get; }

        public MailProcessor(
            IConfigService configService,
            ICustomerMailAddressRepository customerRepository,
            IImapClient imapClient,
            ISmtpClient smtpClient)
        {
            ConfigParams = configService?.ConfigParams ?? throw new ArgumentNullException(nameof(imapClient), "The config service must not be null.");
            CustomerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository), "The customer mail address repository must not be null.");
            ImapClient = imapClient ?? throw new ArgumentNullException(nameof(imapClient), "The imap client must not be null.");
            SmtpClient = smtpClient ?? throw new ArgumentNullException(nameof(smtpClient), "The smtp client must not be null.");
        }

        public void ProcessMails()
        {
            try
            {
                Trace.TraceInformation("Processing mails ...");
                ConnectImap(ImapClient, ConfigParams.Imap);
                ConnectSmtp(SmtpClient, ConfigParams.Smtp);

                var newMessageEnvelopes = ImapClient.GetNewValidMails();
                foreach (var messageEnvelope in newMessageEnvelopes)
                {
                    MailboxAddress customerMailAddress = null;
                    try
                    {
                        customerMailAddress = GetCustomerMailAddress(messageEnvelope.Message.To);
                        if (customerMailAddress == null)
                        {
                            Trace.TraceWarning($"Could not found the to address: <{messageEnvelope.Message.To}> in the order data repository, perhaps the order is from today?");
                            // leave the mail in the inbox for the next run
                            continue;
                        }
                        SmtpClient.ForwardMail(messageEnvelope.Message, customerMailAddress);
                        ImapClient.MoveMailToDone(messageEnvelope.Id);
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError($"Could not process mail to {customerMailAddress}, trying to continue. Reason: {ex}");
                    }
                }
                Trace.TraceInformation("Processed all mails.");
            }
            finally
            {
                ImapClient.Dispose();
                SmtpClient.Dispose();
            }

        }

        private void ConnectSmtp(ISmtpClient smtpClient, IConnectionData connectionData)
        {
            smtpClient.Connect(
                 connectionData.Host,
                 connectionData.Port,
                 connectionData.User,
                 connectionData.Password);

        }

        private void ConnectImap(IImapClient imapClient, IConnectionData connectionData)
        {
            imapClient.Connect(
                 connectionData.Host,
                 connectionData.Port,
                 connectionData.User,
                 connectionData.Password);
        }

        private MailboxAddress GetCustomerMailAddress(InternetAddressList toAddresses)
        {
            if (!(toAddresses[0] is MailboxAddress orderAddress)) throw new ArgumentOutOfRangeException(nameof(toAddresses), toAddresses, "The to-addresses have to be of type MailboxAddress.");

            var orderData = CustomerRepository.GetDataByOrderMailAddress(orderAddress.Address);
            if (orderData == null) return null;

            var customerMailAddress = new MailboxAddress(orderData.CustomerMailAddress);

            return customerMailAddress;
        }
    }
}
