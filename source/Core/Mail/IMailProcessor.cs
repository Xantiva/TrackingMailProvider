﻿namespace Xantiva.TrackingMailProvider.Core.Mail
{
    public interface IMailProcessor
    {
        void ProcessMails();
    }
}