﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xantiva.TrackingMailProvider.Customers.Entities;

namespace Xantiva.TrackingMailProvider.Customers.Persistence.Tests
{
    [TestClass]
    public class CustomerMailAddressRepositoryTests
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void Constructor()
        {
            var sut = new CustomerMailAddressRepository(GetConnectionString("Constructor"));
        }

        [TestMethod]
        public void Insert_ValidValue()
        {
            // arrange
            var sut = new CustomerMailAddressRepository(GetConnectionString("Insert_ValidValue"));
            var expected = new OrderMailData
            {
                Id = "AU-123",
                OrderMailAddress = "AU-123@example.com",
                CustomerMailAddress = "mail@example.com",
                OrderDate = DateTime.Now.Date
            };

            // act
            sut.Insert(expected);

            // assert
            OrderMailData actual = sut.GetDataByOrderMailAddress(expected.OrderMailAddress);
            AssertOrderMailData(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Insert_NullValue()
        {
            // arrange
            var sut = new CustomerMailAddressRepository(GetConnectionString("Insert_NullValue"));

            // act
            sut.Insert(null);
        }


        [TestMethod]
        [ExpectedException(typeof(LiteDB.LiteException))]
        public void Insert_SameIdFails()
        {
            // arrange
            var sut = new CustomerMailAddressRepository(GetConnectionString("Insert_NullValue"));
            var firstData = new OrderMailData
            {
                Id = "AU-123",
                OrderMailAddress = "AU-123@example.com",
                CustomerMailAddress = "mail@example.com",
                OrderDate = DateTime.Now.Date
            };
            var sameId = new OrderMailData
            {
                Id = "AU-123",
                OrderMailAddress = "AU-123@example.de",
                CustomerMailAddress = "mail@example.de",
                OrderDate = DateTime.Now.Date.AddDays(-2)
            };

            // act
            sut.Insert(firstData);
            sut.Insert(sameId);
        }



        [TestMethod]
        public void InsertBulk()
        {
            // arrange
            var sut = new CustomerMailAddressRepository(GetConnectionString("InsertBulk"));
            var expected1 = new OrderMailData
            {
                Id = "AU-427",
                OrderMailAddress = "AU-427@example.com",
                CustomerMailAddress = "mail1@example.com",
                OrderDate = DateTime.Now.Date
            };
            var expected2 = new OrderMailData
            {
                Id = "AU-431",
                OrderMailAddress = "AU-431@example.com",
                CustomerMailAddress = "mail2@example.com",
                OrderDate = DateTime.Now.Date
            };
            var expected3 = new OrderMailData
            {
                Id = "BS-3243",
                OrderMailAddress = "BS-3243@example.com",
                CustomerMailAddress = "mail3@example.com",
                OrderDate = DateTime.Now.Date
            };
            var expected = new List<OrderMailData> { expected1, expected2, expected3 };

            // act
            sut.InsertBulk(expected);

            // assert
            OrderMailData actual = sut.GetDataByOrderMailAddress(expected1.OrderMailAddress);
            AssertOrderMailData(expected1, actual);
            actual = sut.GetDataByOrderMailAddress(expected2.OrderMailAddress);
            AssertOrderMailData(expected2, actual);
            actual = sut.GetDataByOrderMailAddress(expected3.OrderMailAddress);
            AssertOrderMailData(expected3, actual);
        }


        [TestMethod]
        public void DeleteItemsOlderThan()
        {
            // arrange
            var sut = new CustomerMailAddressRepository(GetConnectionString("DeleteItemsOlderThan"));
            var expected1 = new OrderMailData
            {
                Id = "AU-427",
                OrderMailAddress = "AU-427@example.com",
                CustomerMailAddress = "mail1@example.com",
                OrderDate = DateTime.Now.Date.AddDays(-25)
            };
            var expected2 = new OrderMailData
            {
                Id = "AU-431",
                OrderMailAddress = "AU-431@example.com",
                CustomerMailAddress = "mail2@example.com",
                OrderDate = DateTime.Now.Date.AddDays(-35)
            };
            var expected3 = new OrderMailData
            {
                Id = "BS-3243",
                OrderMailAddress = "BS-3243@example.com",
                CustomerMailAddress = "mail3@example.com",
                OrderDate = DateTime.Now.Date.AddDays(-40)
            };
            var expected = new List<OrderMailData> { expected1, expected2, expected3 };
            sut.InsertBulk(expected);
            var thirtyDays = 30;

            // act
            var deleted = sut.DeleteItemsOlderThan(thirtyDays);

            // assert
            OrderMailData actual = sut.GetDataByOrderMailAddress(expected1.OrderMailAddress);
            AssertOrderMailData(expected1, actual);
            actual = sut.GetDataByOrderMailAddress(expected2.OrderMailAddress);
            Assert.IsNull(actual);
            actual = sut.GetDataByOrderMailAddress(expected3.OrderMailAddress);
            Assert.IsNull(actual);
            Assert.AreEqual(2, deleted);
        }

        #region Helper

        private string GetConnectionString(string method)
        {
            var filename = Path.Combine(TestContext.TestDir, $"CustomerMailAddressRepositoryTests.{method}.db");
            var connectionString = $"Filename={filename}";
            return connectionString;
        }

        private static void AssertOrderMailData(OrderMailData expected, OrderMailData actual)
        {
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.OrderMailAddress, actual.OrderMailAddress);
            Assert.AreEqual(expected.CustomerMailAddress, actual.CustomerMailAddress);
            Assert.AreEqual(expected.OrderDate, actual.OrderDate);
        }

        #endregion
    }
}
