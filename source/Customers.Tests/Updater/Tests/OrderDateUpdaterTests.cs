﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xantiva.TrackingMailProvider.Customers.Import;
using Xantiva.TrackingMailProvider.Customers.Persistence;
using Xantiva.TrackingMailProvider.Shared.Configuration;

namespace Xantiva.TrackingMailProvider.Customers.Updater.Tests
{
    [TestClass]
    public class OrderDateUpdaterTests
    {
        public OrderDateUpdater Sut { get; set; }

        protected Mock<IConfigService> ConfigServiceMock { get; private set;}
        protected Mock<IOrderDataImporter> ImporterMock { get; private set; }
        protected Mock<ICustomerMailAddressRepository> RepositoryMock { get; private set; }

        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void SetTestUp()
        {
            ConfigServiceMock = new Mock<IConfigService>();
            ImporterMock = new Mock<IOrderDataImporter>();
            RepositoryMock = new Mock<ICustomerMailAddressRepository>();

            Sut = new OrderDateUpdater(ConfigServiceMock.Object, ImporterMock.Object, RepositoryMock.Object);
        }

        [TestMethod]
        public void UpdateRepository()
        {
            // arrange
            var repoFile = Path.Combine(TestContext.TestRunDirectory, "OrderDateUpdaterTests.UpdateRepository");
            var connectionString = $"Filename={repoFile}";
            var repository = new CustomerMailAddressRepository(connectionString);
            var importer = new JtlOrderDataImporter();
            var testDataFolder = Directory.CreateDirectory(Path.Combine(TestContext.TestRunDirectory, "OrderDateUpdaterTests.DataPath"));
            ConfigServiceMock
                .SetupGet(p => p.Importfolder)
                .Returns(testDataFolder.FullName);
            var sut = new OrderDateUpdater(ConfigServiceMock.Object, importer, repository);
            var dataFile = Path.Combine(Environment.CurrentDirectory, "Import", "Data", "JTL-Export-Auftraege-Test.csv");
            var importedFile = Path.Combine(testDataFolder.FullName, "data.csv");
            File.Copy(dataFile, importedFile);

            // act
            sut.UpdateRepository();

            // assert
            Assert.AreEqual("c.s@example.com", repository.GetDataByOrderMailAddress("BS-12886@bastelnselbermachen.de").CustomerMailAddress);
            Assert.IsFalse(File.Exists(importedFile));

        }
    }
}
