﻿using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Xantiva.TrackingMailProvider.Customers.Import.Tests
{
    [TestClass]
    public class JtlOrderDataImporterTests
    {
        public JtlOrderDataImporter Sut { get; set; }


        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void SetTestUp()
        {
            Sut = new JtlOrderDataImporter();
        }


        [TestMethod]
        public void Import_FromExportAmeise()
        {
            // Die CSV wird inzwischen ohne eine Headerzeile erwartet!

            // arrange
            var filename = Path.Combine(Environment.CurrentDirectory, "Import", "Data", "JTL-Export-Auftraege-Test.csv");

            // act
            var result = Sut.Import(filename);

            // assert
            var list = result.ToList();

            Assert.AreEqual(4, list.Count);
            var validItem1 = list.Where(p => p.Id == "AU-818").FirstOrDefault();
            Assert.IsNotNull(validItem1);
            Assert.AreEqual("AU-818@bastelnselbermachen.de".ToLower(), validItem1.OrderMailAddress);
            Assert.AreEqual("k.s@example.com", validItem1.CustomerMailAddress);
            Assert.AreEqual(DateTime.Parse("06.06.2018"), validItem1.OrderDate);

            var invalidItem1 = list.Where(p => p.Id == "B-1-10022").FirstOrDefault();
            Assert.IsNull(invalidItem1);
            var invalidItem2 = list.Where(p => p.Id == "AU-824").FirstOrDefault();
            Assert.IsNull(invalidItem2);
        }

        [TestMethod]
        public void Import_FromExportWorkflow()
        {
            var filename = Path.Combine(Environment.CurrentDirectory, "Import", "Data", "Auftraege-per-Workflow.csv");
            var result = Sut.Import(filename);
            var list = result.ToList();

            Assert.AreEqual(13, list.Count);
            var validItem1 = list.Where(p => p.Id == "BS-13006").FirstOrDefault();
            Assert.IsNotNull(validItem1);
            Assert.AreEqual("BS-13006@bastelnselbermachen.de".ToLower(), validItem1.OrderMailAddress);
            Assert.AreEqual("xxxx@gmx.net", validItem1.CustomerMailAddress);
            Assert.AreEqual(DateTime.Parse("21.06.2018 09:07:48"), validItem1.OrderDate);
        }
    }
}
